﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //查找
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from Students where sname like '%{0}%';",name);
            var data = DBhelper.GetDataTable(sql);
            dataGridView1.DataSource = data;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var sql = " select * from Students;";
            var data = DBhelper.GetDataTable(sql);
            dataGridView1.DataSource = data;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }
        //添加
        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            var res = form2.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var aaa = " select * from Students;";
                var data = DBhelper.GetDataTable(aaa);
                dataGridView1.DataSource = data;

            }
            else
            {
                MessageBox.Show("取消这次添加", "提示");
            }
        }
        //更新
        private void button3_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["id"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["sname"].Value;
            var age= (int)dataGridView1.SelectedRows[0].Cells["sage"].Value;
            var sex = (string)dataGridView1.SelectedRows[0].Cells["sex"].Value;
            var score= (int)dataGridView1.SelectedRows[0].Cells["score"].Value;
            Form2 form2 = new Form2(id, name, age, sex, score);
            var res = form2.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var aaa = " select * from Students;";
                var data = DBhelper.GetDataTable(aaa);
                dataGridView1.DataSource = data;

            }
            else
            {
                MessageBox.Show("取消这次更新","提示");
            }
        }
        //删除
        private void button4_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["id"].Value;
            var sql = string.Format("delete from Students where id ={0}", id);
            var res = DBhelper.AddorUpdateorDelect(sql);
            if (res > 0)
            {
                
                MessageBox.Show("删除成功", "提示");
                var aaa = " select * from Students;";
                var data = DBhelper.GetDataTable(aaa);
                dataGridView1.DataSource = data;
            }
            else
            {
                
                MessageBox.Show("取消了这次删除", "提示");
                var aaa = " select * from Students;";
                var data = DBhelper.GetDataTable(aaa);
                dataGridView1.DataSource = data;
            }
        }
    }
    }
