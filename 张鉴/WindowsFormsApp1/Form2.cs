﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        //取消
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            this.DialogResult = DialogResult.No;

        }
        private int id;
        public Form2 (int id , string name ,int age, string sex,int score)
        {
            InitializeComponent();
            this.id = id;
            textBox1.Text = name;
            textBox2.Text = age.ToString();
            textBox3.Text = sex;
            textBox4.Text = score.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var age = textBox2.Text;
            var sex = textBox3.Text;
            var score = textBox4.Text;
            if (this.id >0)
            {
                var sql = string.Format(" update Students set sname='{0}',sage='{1}',sex='{2}',score='{3}'where id ={4}", name,age,sex,score,this.id);
                DBhelper.AddorUpdateorDelect(sql);
                MessageBox.Show("更新成功", "提示");
            }
            else
            {
                var sql = string.Format("insert into Students(sname,sage,sex,score) values('{0}','{1}','{2}','{3}')",name,age,sex,score);
                DBhelper.AddorUpdateorDelect(sql);
                MessageBox.Show("添加成功", "提示");
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
    }
}
